# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of be.po to Belarusian (Official spelling)
# Andrei Darashenka <adorosh2@it.org.by>, 2005, 2006.
# Nasciona Piatrouskaja <naska.pet@gmail.com>, 2006.
# Pavel Piatruk <berserker@neolocation.com>, 2006, 2007, 2008.
# Hleb Rubanau <g.rubanau@gmail.com>, 2006, 2007.
# Nasciona Piatrouskaja <naska1@tut.by>, 2006.
# Paul Petruk <berserker@neolocation.com>, 2007.
# Pavel Piatruk <piatruk.p@gmail.com>, 2008, 2009, 2011.
# Viktar Siarheichyk <vics@eq.by>, 2010, 2011, 2012, 2015.
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@debian.org>, 2004.
# Alexander Nyakhaychyk <nyakhaychyk@gmail.com>, 2009.
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2007, 2010.
# Viktar Siarheichyk <viсs@eq.by>, 2014.
# Viktar Siarheichyk <vics@fsfe.org>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: be\n"
"Report-Msgid-Bugs-To: nobootloader@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:04+0000\n"
"PO-Revision-Date: 2015-12-21 15:13+0300\n"
"Last-Translator: Viktar Siarheichyk <vics@fsfe.org>\n"
"Language-Team: Debian l10n team for Belarusian <debian-l10n-belarusian@lists."
"debian.org>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../nobootloader.templates:1001
msgid "Continue without boot loader"
msgstr "Працягваць без загрузчыка"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Failed to mount /target/proc"
msgstr "Не атрымалася прымацаваць /target/proc"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Mounting the proc file system on /target/proc failed."
msgstr ""
"Не атрымалася прымацаваць файлавую сістэму proc да пункта /target/proc."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "Падрабязнасці чытайце ў /var/log/syslog і на віртуальнай кансолі 4."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Warning: Your system may be unbootable!"
msgstr "Увага: Вашая сістэма можа быць няздольная да загрузкі!"

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
msgid "Setting firmware variables for automatic boot"
msgstr "Наладка параметраў прашыўкі для аўтаматычнай загрузкі"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""
"Варта вызначыць некаторыя параметры для прашыўкі Genesi, каб Вашая сістэма "
"магла аўтаматычна загружацца. Напрыканцы ўстаноўкі сістэма перазагрузіцца. У "
"абалонцы прашыўкі вызначце наступныя параметры, каб дазволіць аўтаматычную "
"загрузку: "

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"Вам трэба зрабіць гэта толькі аднойчы. Потым увядзіце каманду \"boot\" або "
"іншым чынам перазагрузіце сістэму, каб пачаць працу з Debian. "

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr "Як варыянт, Вы можаце загружаць ядро самастойна, уводзячы: "

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"Варта вызначыць некаторыя параметры ў CFE, каб Вашая сістэма магла "
"аўтаматычна загружацца. Напрыканцы ўстаноўкі сістэма перазагрузіцца. У "
"абалонцы прашыўкі вызначце наступныя параметры, каб дазволіць аўтаматычную "
"загрузку: "

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""
"Вам трэба зрабіць гэта аднойчы. Проста запусціць каманду \"boot_debian\" ў "
"абалонцы CFE."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""
"Калі вы аддаеце перавагу аўта-загрузцы пры кожным запуску, то можаце "
"ўстанавіць наступныя пераменныя ў дадатак да тых, што вышэй:"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid "No boot loader installed"
msgstr "Не ўсталявана ніякага загрузчыка"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid ""
"No boot loader has been installed, either because you chose not to or "
"because your specific architecture doesn't support a boot loader yet."
msgstr ""
"Не ўсталявана ніякага загрузчыка. Магчыма, гэта быў Ваш выбар пры ўстаноўцы. "
"У іншым выпадку гэта значыць, што Ваша архітэктура не падтрымлівае загрузчык."

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:5001
msgid ""
"You will need to boot manually with the ${KERNEL} kernel on partition "
"${BOOT} and ${ROOT} passed as a kernel argument."
msgstr ""
"Вам трэба самастойна загрузіць ядро ${KERNEL} з падзела ${BOOT}, перадаўшы "
"радок ${ROOT} у якасці параметра ядра."
